json.array!(@manages) do |manage|
  json.extract! manage, :id, :user_id, :plan, :message, :books, :date, :user_id
  json.url manage_url(manage, format: :json)
end
