class BoxesController < ApplicationController
  before_action :set_box, only: [:show, :edit, :update, :destroy]

  
  # GET /boxes
  # GET /boxes.json
  def index
    @boxes = Box.all
    
  end
# Returns the current logged-in user (if any).
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  # GET /boxes/1
  # GET /boxes/1.json
  def show
    id = Hashids.new("black napkins", 8).decode(params[:id]).try(:first)
    @boxes = Box.find(id)
    @microposts = Micropost.where(:boxes_id => @boxes.id)
    @user = current_user
    @micropost = current_user.microposts
  end

 
  # GET /boxes/new
  def new
    @box = Box.new
  end

  # GET /boxes/1/edit
  def edit
  end

  # POST /boxes
  # POST /boxes.json
  def create
    @box = Box.new(box_params)

    respond_to do |format|
      if @box.save
        format.html { redirect_to @box, notice: 'Book was successfully created.' }
        format.json { render :show, status: :created, location: @box }
      else
        format.html { render :new }
        format.json { render json: @box.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /boxes/1
  # PATCH/PUT /boxes/1.json
  def update
    respond_to do |format|
      if @box.update(box_params)
        format.html { redirect_to @box, notice: 'Book was successfully updated.' }
        format.json { render :show, status: :ok, location: @box }
      else
        format.html { render :edit }
        format.json { render json: @box.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /boxes/1
  # DELETE /boxes/1.json
  def destroy
    
    @box.destroy
    respond_to do |format|
      format.html { redirect_to current_user, notice: 'Book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_box
      id = Hashids.new("black napkins", 8).decode(params[:id]).try(:first)
      @box = Box.find(id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def box_params
      params.require(:box).permit(:book, :user_id)
    end
end
