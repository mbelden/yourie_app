class MicropostsController < ApplicationController
  
  
  def new
    @micropost = Micropost.new
    
  end

  def create
        @micropost = current_user.microposts.build(micropost_params)
    
    if @micropost.save
      flash[:success] = "Yourie Post Created!"
      redirect_to(:back)
    else
      
      render 'static_pages/home'
    end
  end

  def edit
      
      @micropost = Micropost.find(params[:id])
      session[:prev_url] = request.referer
  end
  
  def update
    @micropost = Micropost.find(params[:id])
    if @micropost.update_attributes(micropost_params)
      
      flash[:success] = "Yourie Post Updated!"
      redirect_to session[:prev_url]
       
   else
      render 'edit'
    end
  end

  def destroy
    @micropost = Micropost.find(params[:id])
    @micropost.destroy
    flash[:success] = "Yourie Post Deleted!"
    redirect_to request.referrer || root_url
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content, :boxes_id, :user_id, :picture)
    end
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
