require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Yourie" 
    assert_equal full_title("YouriePay"), "YouriePay | Yourie" 
  end
end