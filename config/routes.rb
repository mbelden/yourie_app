Rails.application.routes.draw do
  
  resources :manages
  resources :boxes
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'
  
  root 'static_pages#home'
  get 'help'    => 'static_pages#help'
  get 'basic'    => 'static_pages#basic'
  get 'packages'    => 'static_pages#packages'
  get 'vault'    => 'static_pages#vault'
  get 'suggestions'    => 'static_pages#suggestions'
  get 'sample'    => 'static_pages#sample'
  get 'deleteaccount'    => 'static_pages#delete_account'
  get 'premium'    => 'static_pages#premium'
  get 'about'   => 'static_pages#about'
  get 'giftshop'   => 'static_pages#giftshop'
  get 'contact' => 'static_pages#contact'
  get 'terms' => 'static_pages#terms'
  get 'invite'    => 'static_pages#invite'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  
  resources :users do
    member do
      get :following, :followers
    end
  end
  
  
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :boxes,          only: [:create, :destroy]
  resources :microposts,          only: [:create, :destroy]
  
  
  resources :relationships,       only: [:create, :destroy]
  resources :boxes do
  resources :microposts
    end
  
  resources :users do
  resources :microposts
    end
  resources :users do
  resources :boxes
    end
  resources :boxes do
  resources :users
    end
  resources :microposts do
  resources :users
    end
  resources :microposts do
  resources :boxes
    end
  
  resources :users,                only: [:edit, :create, :destroy]
  resources :boxes,                only: [:edit, :create, :destroy]
  resources :reverses
  resources :manages
  resources :microposts,          only: [:edit, :create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  #root 'application#hello'
  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
