class CreateBoxes < ActiveRecord::Migration
  def change
    create_table :boxes do |t|
      t.text :book
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
