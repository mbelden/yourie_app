class AddPlanAndStatusAndMessageAndBooksAndDateToUser < ActiveRecord::Migration
  def change
    add_column :users, :plan, :string
    add_column :users, :status, :string
    add_column :users, :message, :text
    add_column :users, :books, :integer
    add_column :users, :date, :timestamp
  end
end
