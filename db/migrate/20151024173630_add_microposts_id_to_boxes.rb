class AddMicropostsIdToBoxes < ActiveRecord::Migration
  def change
    add_column :boxes, :microposts_id, :integer
  end
end
