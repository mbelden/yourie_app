User.create!(name:  "Yourie",
             email: "mab.yourie@gmail.com",
             password:              "yc361212D1212",
             password_confirmation: "yc361212D1212",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name:  "Mark Belden",
             email: "mbeldendev@gmail.com",
             password:              "belden1212",
             password_confirmation: "belden1212",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name:  "Kelly Belden",
             email: "kbeldendev@gmail.com",
             password:              "belden1212",
             password_confirmation: "belden1212",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Nonnie Poppie",
             email: "nonniepoppie20@gmail.com",
             password:              "nonpop2020",
             password_confirmation: "nonpop2020",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Belden Family",
             email: "beldens.yourie@gmail.com",
             password:              "belden1212",
             password_confirmation: "belden1212",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name:  "Test 1",
             email: "test1@gmail.com",
             password:              "belden1212",
             password_confirmation: "belden1212",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name:  "Test 2",
             email: "test2@gmail.com",
             password:              "belden1212",
             password_confirmation: "belden1212",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name:  "Test 3",
             email: "test3@gmail.com",
             password:              "belden1212",
             password_confirmation: "belden1212",
             admin:     false,
             activated: true,
             activated_at: Time.zone.now)



10.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end


